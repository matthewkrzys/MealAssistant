package mealassistant.mealassistant;



import mealassistant.mealassistant.model.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

public class ModelTest {



    @Test
    public void DataXMLTwoParamTest(){
        DataXML dataXML=new DataXML("a","b");
        Assert.assertTrue(dataXML.getName().equals("a"));
    }

    @Test
    public void DataXMLThreeParamTest(){
        DataXML dataXML=new DataXML("a","b","c");
        Assert.assertTrue(dataXML.getValueTime().equals("c"));
    }

    @Test
    public void HistoryTest(){
        History history=new History();
        history.setUserid(1L);
        history.setMealsid(2L);
        history.setMealTime("a");
        history.setPostedAt(new Date());
        Assert.assertTrue(history.getMealTime().equals("a"));
    }

    @Test
    public void MealsTest(){
        Meals meals=new Meals();
        meals.setName("a");
        meals.setEnergy(2);
        meals.setProtein(3);
        meals.setFats(4);
        meals.setCarbohydrates(5);
        meals.setIdProducts("1,2");
        meals.setMealTime("B");
        Assert.assertTrue(meals.getIdProducts().contains("1"));
    }

    @Test
    public void ProductsTest(){
        Products products=new Products();
        products.setIdFromAPI("1234");
        products.setName("A");
        products.setEnergy(1);
        products.setProtein(2);
        products.setFats(3);
        products.setCarbohydrates(4);
        Assert.assertTrue(products.getName().equals("A"));
    }

    @Test
    public void UserTest(){
        User user=new User();
        user.setName("A");
        user.setSurname("B");
        user.setLogin("C");
        user.setPassword("D");
        user.setAge(1);
        user.setWeight(2);
        user.setGrowth(3);
        user.setProtein(4);
        user.setFats(5);
        user.setCarbohydrates(6);
        Assert.assertTrue(user.getCarbohydrates()==6);
    }

    @Test
    public void ApplicationUserTest(){
        ApplicationUser user=new ApplicationUser();
        user.setUsername("A");
        user.setPassword("B");
        Assert.assertTrue(user.getPassword().equals("B"));
    }
}
