package mealassistant.mealassistant;


import mealassistant.mealassistant.logic.DataFromAPI;
import org.junit.Assert;
import org.junit.Test;

public class DataFromAPITest {

    DataFromAPI dataFromAPI;

    @Test
    public void getDataTest() {
        dataFromAPI=new DataFromAPI();
        String result=dataFromAPI.getData("09040");
        Assert.assertTrue(result.contains("09040"));
    }
}
