package mealassistant.mealassistant;

import mealassistant.mealassistant.model.History;
import mealassistant.mealassistant.model.Meals;
import mealassistant.mealassistant.repository.HistoryRepository;
import mealassistant.mealassistant.repository.MealsRepository;
import mealassistant.mealassistant.repository.UserRepository;
import mealassistant.mealassistant.service.Manager;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.Silent.class)
public class ManagerTest {

    @Mock
    Manager manager;

    @Autowired
    MealsRepository mealsRepository;

    @Autowired
    HistoryRepository historyRepository;

    @Autowired
    UserRepository userRepository;

    @Test
    public void getTimeMealTest() {
        List<String> historyList=new ArrayList<>();
        when(manager.getMealToEat(any(String.class),any(MealsRepository.class),any(HistoryRepository.class)))
                .thenReturn(historyList);
        List e=manager.getMealToEat("1",mealsRepository,historyRepository);
        Assert.assertTrue(e.size()==0);

    }

    @Test
    public void getStatusTest(){
        List<String> list=new ArrayList<>();
        when(manager.getStaus(any(String.class),any(HistoryRepository.class),any(UserRepository.class),any(MealsRepository.class))).thenReturn(list);
        List e=manager.getStaus("1",historyRepository,userRepository,mealsRepository);
        Assert.assertTrue(e.size()==0);
    }

    @Test
    public void addMealToHistoryTest(){
        List<String> list=new ArrayList<>();
        when(manager.addMealToHistory(any(String.class),any(String.class),any(HistoryRepository.class))).thenReturn(list);
        List e=manager.addMealToHistory("1","2",historyRepository);
        Assert.assertTrue(e.size()==0);
    }
    @Test
    public void getHistoryForUserTest(){
        List<Meals> list=new ArrayList<>();
        when(manager.getHistoryForUser(any(String.class),any(HistoryRepository.class),any(MealsRepository.class))).thenReturn(list);
        List e=manager.getHistoryForUser("1",historyRepository,mealsRepository);
        Assert.assertTrue(e.size()==0);
    }
    @Test
    public void getHistoryTest(){
        List<History> list=new ArrayList<>();
        when(manager.getHistory(any(String.class),any(HistoryRepository.class))).thenReturn(list);
        List e=manager.getHistory("1",historyRepository);
        Assert.assertTrue(e.size()==0);
    }
}
