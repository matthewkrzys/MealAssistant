package mealassistant.mealassistant;


import mealassistant.mealassistant.logic.ReadFromXML;
import mealassistant.mealassistant.model.DataXML;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ReadFromXMLTest {

    @Mock
    ReadFromXML readFromXML;

    @Test
    public void readXMLTest(){
        List<DataXML> elementList=new ArrayList<>();
        when(readFromXML.readXML(any(String.class),any(String.class),any(String.class),any(String.class)))
                .thenReturn(elementList);
        List e=readFromXML.readXML("products.xml", "product", "name", "id");
        Assert.assertTrue(e.size()==0);
    }

    @Test
    public void readXMLTimeValueTest(){
        List<DataXML> elementList=new ArrayList<>();
        when(readFromXML.readXMLTimeValue(any(String.class),any(String.class),any(String.class),any(String.class),any(String.class)))
                .thenReturn(elementList);
        List e=readFromXML.readXMLTimeValue("meals.xml", "meal", "name", "products", "mealTime");;
        Assert.assertTrue(e.size()==0);
    }

    @Test
    public void GetNodeListTest(){
        NodeList nodeList=null;
        when(readFromXML.GetNodeList(any(String.class),any(String.class)))
                .thenReturn(nodeList);
        NodeList e=readFromXML.GetNodeList("nutrient", "data");
        Assert.assertTrue(e==null);
    }

}
