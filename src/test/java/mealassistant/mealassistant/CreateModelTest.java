package mealassistant.mealassistant;

import mealassistant.mealassistant.logic.CreateModel;
import mealassistant.mealassistant.model.Products;
import mealassistant.mealassistant.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.w3c.dom.NodeList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class CreateModelTest {
    @Mock
    CreateModel createModel;

    @Mock
    NodeList nodeList;

    @Test
    public void createUserTest() {
        createModel=new CreateModel();
        User u = createModel.createUser(new User("Adam", "Ad", "aadam", "alamakota", 21, 60.5, 175.4, 0, 0, 0));
        Assert.assertTrue(u.getCarbohydrates() > 0);
    }

    @Test
    public void addToObjectTest() {
        Products products = new Products();
        products.setIdFromAPI("1234");
        products.setName("A");
        products.setEnergy(1);
        products.setProtein(2);
        products.setFats(3);
        products.setCarbohydrates(4);
        when(createModel.addToObject(any(NodeList.class), any(String.class), any(String.class))).thenReturn(products);
        Products p = createModel.addToObject(nodeList, "1", "2");
        Assert.assertTrue(p.getName().equals("A"));
    }

}
