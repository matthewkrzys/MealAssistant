package mealassistant.mealassistant.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "history")
public class History {
//    private static Long idGenerate=0L;
    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Getter
    @Setter
    @NotNull
    private Long userid;

    @Getter
    @Setter
    @NotNull
    private Long mealsid;

    @Getter
    @Setter
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "posted_at")
    private Date postedAt = new Date();

    @Getter
    @Setter
    @NotNull
    private String mealTime;


    public History() {
    }

    public History(@NotNull Long userid, @NotNull Long mealsid) {
        this.userid = userid;
        this.mealsid = mealsid;
    }
}
