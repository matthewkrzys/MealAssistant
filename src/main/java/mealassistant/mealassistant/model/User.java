package mealassistant.mealassistant.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "user")
public class User{
    private static Long idGenerate=0L;
    @Getter
    @Setter
    @Id
//    @GeneratedValue
    private Long id=idGenerate++;


    @Getter
    @Setter
    @NotNull
    private String name;

    @Getter
    @Setter
    @NotNull
    private String surname;

    @Getter
    @Setter
    @NotNull
    private String login;

    @Getter
    @Setter
    @NotNull
    private String password;

    @Getter
    @Setter
    @NotNull
    private int age;

    @Getter
    @Setter
    @NotNull
    private double weight;

    @Getter
    @Setter
    @NotNull
    private double growth;

    @Getter
    @Setter
    @NotNull
    private double protein;

    @Getter
    @Setter
    @NotNull
    private double fats;

    @Getter
    @Setter
    @NotNull
    private double carbohydrates;

    public User() {
    }

    public User(@NotNull String name, @NotNull double protein, @NotNull double fats, @NotNull double carbohydrates) {
        this.name = name;
        this.protein = protein;
        this.fats = fats;
        this.carbohydrates = carbohydrates;
    }

    public User(@NotNull String name, @NotNull String surname, @NotNull String login, @NotNull String password, @NotNull int age, @NotNull double weight, @NotNull double growth, @NotNull double protein, @NotNull double fats, @NotNull double carbohydrates) {
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.password = password;
        this.age = age;
        this.weight = weight;
        this.growth = growth;
        this.protein = protein;
        this.fats = fats;
        this.carbohydrates = carbohydrates;
    }

    public User(@NotNull String name, @NotNull String surname, @NotNull String login, @NotNull String password) {
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.password = password;
    }


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
/*
{"name":"Adam","surname":"Ad","login":"aadam","password":"alamakota","age":21,"weight":60.5,"growth":50.4,"protein":60.5,"fats":60.5,"carbohydrates":60.5}
 */