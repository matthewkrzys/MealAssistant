package mealassistant.mealassistant.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "meals")
public class Meals {
    private static Long idGenerate=1L;
    @Getter
    @Setter
    @Id
    @GeneratedValue
    private Long id;

    @Getter
    @Setter
    @NotNull
    private String name;

    @Getter
    @Setter
    @NotNull
    private double energy;

    @Getter
    @Setter
    @NotNull
    private double protein;

    @Getter
    @Setter
    @NotNull
    private double fats;

    @Getter
    @Setter
    @NotNull
    private double carbohydrates;

    @Getter
    @Setter
    @NotNull
    private String idProducts;

    @Getter
    @Setter
    @NotNull
    private String mealTime;

    public Meals() {
    }

    @Override
    public String toString() {
        return "Meals{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", energy=" + energy +
                ", protein=" + protein +
                ", fats=" + fats +
                ", carbohydrates=" + carbohydrates +
                ", idProducts='" + idProducts + '\'' +
                ", mealTime='" + mealTime + '\'' +
                '}';
    }
}
/*
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "posted_at")
    private Date postedAt = new Date();

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_updated_at")
    private Date lastUpdatedAt = new Date();


//    @Getter
//    @Setter
//    @ManyToMany(cascade = CascadeType.ALL)
//    @JoinTable(name = "meals_products",
//            joinColumns = @JoinColumn(name = "meals_id", referencedColumnName = "id"),
//            inverseJoinColumns = @JoinColumn(name = "products_id", referencedColumnName = "id"))
//    private Set<Products> productsSet=new HashSet<>();
dodawajny tylko id produktów podczas dodawanie do bazy do posiłków
 */
