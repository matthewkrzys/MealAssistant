package mealassistant.mealassistant.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "products")
public class Products {
    private static Long idGenerate=0L;
    @Getter
    @Setter
    @Id
//    @GeneratedValue
    private Long id=idGenerate++;

    @Getter
    @Setter
    private String idFromAPI;


    @Getter
    @Setter
    @NotNull
    private String name;

    @Getter
    @Setter
    @NotNull
    private double energy;

    @Getter
    @Setter
    @NotNull
    private double protein;

    @Getter
    @Setter
    @NotNull
    private double fats;

    @Getter
    @Setter
    @NotNull
    private double carbohydrates;


    public Products() {

    }

    public Products(@NotNull String name, @NotNull double protein, @NotNull double fats, @NotNull double carbohydrates) {
        this.name = name;
        this.protein = protein;
        this.fats = fats;
        this.carbohydrates = carbohydrates;
    }

    @Override
    public String toString() {
        return "Products{" +
                "id=" + id +
                ", idFromAPI='" + idFromAPI + '\'' +
                ", name='" + name + '\'' +
                ", energy=" + energy +
                ", protein=" + protein +
                ", fats=" + fats +
                ", carbohydrates=" + carbohydrates +
                '}';
    }
}