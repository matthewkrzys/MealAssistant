package mealassistant.mealassistant.model;

import lombok.Getter;
import lombok.Setter;

public class DataXML {
    @Getter
    @Setter
    public String name;
    @Getter
    @Setter
    public String value;
    @Getter
    @Setter
    public String valueTime;

    public DataXML() {
    }

    public DataXML(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public DataXML(String name, String value, String valueTime) {
        this.name = name;
        this.value = value;
        this.valueTime = valueTime;
    }

    @Override
    public String toString() {
        return "DataXML{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                ", valueTime='" + valueTime + '\'' +
                '}';
    }
}
