package mealassistant.mealassistant.logic;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
@Slf4j
public class DataFromAPI {
    final static Logger logger = LoggerFactory.getLogger(DataFromAPI.class);
    final static String TAG= DataFromAPI.class.getName();
    public String getData(String id){
        logger.info(TAG+" getData ");
        logger.info(TAG+" id "+id);
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet("https://api.nal.usda.gov/ndb/V2/reports?ndbno="+id+"&format=xml&type=b&api_key=W08Sl2iZEFgRanJ17iMfIBNKEBRV3JN1xpVlji0S");
        try {
            HttpResponse response = client.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                try (InputStream stream = entity.getContent()) {
                    BufferedReader reader =
                            new BufferedReader(new InputStreamReader(stream));
                    String dataFromServer = "";
                    String line;
                    while ((line = reader.readLine()) != null) {
                        dataFromServer += line;
                    }
                    logger.info(TAG+" dataFromServer "+dataFromServer);
                    return dataFromServer;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
