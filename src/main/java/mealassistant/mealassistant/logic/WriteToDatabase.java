package mealassistant.mealassistant.logic;

import lombok.extern.slf4j.Slf4j;
import mealassistant.mealassistant.model.Meals;
import mealassistant.mealassistant.model.Products;
import mealassistant.mealassistant.repository.MealsRepository;
import mealassistant.mealassistant.repository.ProductsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@Slf4j
public class WriteToDatabase {

    final static Logger logger = LoggerFactory.getLogger(WriteToDatabase.class);
    final static String TAG= WriteToDatabase.class.getName();

    public void createProducts(ProductsRepository productsRepository, Products products) {
        logger.info(TAG+" createProducts ");
        logger.info(TAG+" products "+products);
        productsRepository.save(products);
    }
    public void createMeals(MealsRepository mealsRepository, Meals meals){
        logger.info(TAG+" createMeals ");
        logger.info(TAG+" meals "+meals);
        mealsRepository.save(meals);
    }
}
