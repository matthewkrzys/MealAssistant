package mealassistant.mealassistant.logic;

import lombok.extern.slf4j.Slf4j;
import mealassistant.mealassistant.model.ApplicationUser;
import mealassistant.mealassistant.repository.ApplicationUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import static java.util.Collections.emptyList;
@Slf4j
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	final static Logger logger = LoggerFactory.getLogger(UserDetailsServiceImpl.class);
	final static String TAG= UserDetailsServiceImpl.class.getName();

	private ApplicationUserRepository applicationUserRepository;

	public UserDetailsServiceImpl(ApplicationUserRepository applicationUserRepository) {
		this.applicationUserRepository = applicationUserRepository;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		logger.info(TAG+" loadUserByUsername ");
		logger.info(TAG+" username "+username);
		ApplicationUser applicationUser = applicationUserRepository.findByUsername(username);
		if (applicationUser == null) {
			throw new UsernameNotFoundException(username);
		}
		return new User(applicationUser.getUsername(), applicationUser.getPassword(), emptyList());
	}
}
