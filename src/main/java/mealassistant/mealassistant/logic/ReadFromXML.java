package mealassistant.mealassistant.logic;


import lombok.extern.slf4j.Slf4j;
import mealassistant.mealassistant.model.DataXML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
@Slf4j
public class ReadFromXML {
    final static Logger logger = LoggerFactory.getLogger(ReadFromXML.class);
    final static String TAG= ReadFromXML.class.getName();

    public List<DataXML> readXML(String path, String TagName, String TagXMLName, String TagXMLValue){
        logger.info(TAG+" readXML ");
        logger.info(TAG+" path "+path);
        logger.info(TAG+" TagName "+TagName);
        logger.info(TAG+" TagXMLName "+TagXMLName);
        logger.info(TAG+" TagXMLValue "+TagXMLValue);
        List<DataXML> dataXMLList=new ArrayList<>();
        try {
            File fXmlFile = new File(path);
            //"products.xml"
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName(TagName);
            //"product"
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    System.out.println(TagXMLName+" : " + eElement.getElementsByTagName(TagXMLName).item(0).getTextContent());
                    System.out.println(TagXMLValue+" : " + eElement.getElementsByTagName(TagXMLValue).item(0).getTextContent());
                    dataXMLList
                            .add(new DataXML(eElement.getElementsByTagName(TagXMLName).item(0).getTextContent(),
                                    eElement.getElementsByTagName(TagXMLValue).item(0).getTextContent()));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info(TAG+" dataXMLList "+dataXMLList);
        return dataXMLList;
    }
    public List<DataXML> readXMLTimeValue(String path, String TagName, String TagXMLName, String TagXMLValue, String TagXMLTimeValue){
        logger.info(TAG+" readXMLTimeValue ");
        logger.info(TAG+" path "+path);
        logger.info(TAG+" TagName "+TagName);
        logger.info(TAG+" TagXMLName "+TagXMLName);
        logger.info(TAG+" TagXMLValue "+TagXMLValue);
        logger.info(TAG+" TagXMLTimeValue "+TagXMLTimeValue);
        List<DataXML> dataXMLList=new ArrayList<>();
        try {
            File fXmlFile = new File(path);
            //"products.xml"
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName(TagName);
            //"product"
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    System.out.println(TagXMLName+" : " + eElement.getElementsByTagName(TagXMLName).item(0).getTextContent());
                    System.out.println(TagXMLValue+" : " + eElement.getElementsByTagName(TagXMLValue).item(0).getTextContent());
                    System.out.println(TagXMLValue+" : " + eElement.getElementsByTagName(TagXMLTimeValue).item(0).getTextContent());
                    dataXMLList
                            .add(new DataXML(eElement.getElementsByTagName(TagXMLName).item(0).getTextContent(),
                                    eElement.getElementsByTagName(TagXMLValue).item(0).getTextContent(),
                                    eElement.getElementsByTagName(TagXMLTimeValue).item(0).getTextContent()));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info(TAG+" dataXMLList "+dataXMLList);
        return dataXMLList;
    }

    public NodeList GetNodeList(String TagName,String dataFromServer){
        logger.info(TAG+" GetNodeList ");
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        Document doc = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(new ByteArrayInputStream(dataFromServer.getBytes()));
        } catch (SAXException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        doc.getDocumentElement().normalize();
        logger.info(TAG+" Root element :" + doc.getDocumentElement().getNodeName());
        NodeList nList = doc.getElementsByTagName(TagName);
        logger.info(TAG+" nList "+nList);
        return nList;
    }
}
//"nutrient"