package mealassistant.mealassistant.logic;


import lombok.extern.slf4j.Slf4j;
import mealassistant.mealassistant.model.Products;
import mealassistant.mealassistant.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
@Slf4j
public class CreateModel {

    final static Logger logger = LoggerFactory.getLogger(CreateModel.class);
    final static String TAG= CreateModel.class.getName();

    public Products addToObject(NodeList nList, String id, String name){
        logger.info(TAG+" addToObject ");
        logger.info(TAG+" NodeList "+nList);
        Products products=new Products();
        products.setName(name);
        products.setIdFromAPI(id);
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            logger.info(TAG+" Node "+nNode);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                if(eElement.getAttribute("name").contains("Energy")){
                    System.out.println("Energy");
                    products.setEnergy(Double.parseDouble(eElement.getAttribute("value")));
                }
                if(eElement.getAttribute("name").contains("Carbohydrate")){
                    System.out.println("Carbohydrate");
                    products.setCarbohydrates(Double.parseDouble(eElement.getAttribute("value")));
                }
                if(eElement.getAttribute("name").contains("Protein")){
                    System.out.println("Protein");
                    products.setProtein(Double.parseDouble(eElement.getAttribute("value")));
                }
                if(eElement.getAttribute("name").contains("fat")){
                    System.out.println("fat");
                    products.setFats(Double.parseDouble(eElement.getAttribute("value")));
                }

            }
            logger.info(TAG+" Product "+products);
        }
        return products;
    }
    public User createUser(User user){
        logger.info(TAG+" createUser ");
        User u=user;
        u.setFats(u.getWeight());
        u.setCarbohydrates(u.getWeight()*5);
        u.setProtein(u.getWeight()*2);
        logger.info(TAG+" User "+u);
        return u;
    }
}
