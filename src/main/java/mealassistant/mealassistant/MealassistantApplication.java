package mealassistant.mealassistant;

import lombok.extern.slf4j.Slf4j;
import mealassistant.mealassistant.logic.ReadFromXML;
import mealassistant.mealassistant.repository.MealsRepository;
import mealassistant.mealassistant.repository.ProductsRepository;
import mealassistant.mealassistant.service.Manager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
@Slf4j
@SpringBootApplication
public class MealassistantApplication {
	final static Logger logger = LoggerFactory.getLogger(ReadFromXML.class);

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Autowired
	private MealsRepository mealsRepository;

	@Autowired
	private ProductsRepository productsRepository;


	public static void main(String[] args) {
//		log.info("Run App");
		logger.info("Run App");
		SpringApplication.run(MealassistantApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(){
		return args -> {
			Manager manager=new Manager();
			manager.CreateProductsTable(productsRepository);
			manager.CreateMealsTable(mealsRepository,productsRepository);
		};
	}
}
