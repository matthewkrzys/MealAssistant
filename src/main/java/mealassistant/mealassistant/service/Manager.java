package mealassistant.mealassistant.service;


import lombok.extern.slf4j.Slf4j;
import mealassistant.mealassistant.logic.CreateModel;
import mealassistant.mealassistant.logic.DataFromAPI;
import mealassistant.mealassistant.logic.ReadFromXML;
import mealassistant.mealassistant.logic.WriteToDatabase;
import mealassistant.mealassistant.model.*;
import mealassistant.mealassistant.repository.HistoryRepository;
import mealassistant.mealassistant.repository.MealsRepository;
import mealassistant.mealassistant.repository.ProductsRepository;
import mealassistant.mealassistant.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.w3c.dom.NodeList;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
@Slf4j
@Service
public class Manager {

    final static Logger logger = LoggerFactory.getLogger(Manager.class);
    final static String TAG= Manager.class.getName();

    private ReadFromXML readFromXML;
    private DataFromAPI dataFromAPI;
    private CreateModel createModel;
    private WriteToDatabase writeToDatabase;
    private static final DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public Manager() {
        readFromXML = new ReadFromXML();
        dataFromAPI = new DataFromAPI();
        createModel = new CreateModel();
        writeToDatabase = new WriteToDatabase();
    }

    public void CreateProductsTable(ProductsRepository productsRepository) {
        logger.info(TAG+" CreateProductsTable ");
        logger.info(TAG+" productsRepository "+productsRepository);
        NodeList nodeList;
        Products products;
        List<DataXML> list = readFromXML.readXML("products.xml", "product", "name", "id");
        logger.info(TAG+" List<DataXML> "+list);
        for (DataXML d : list
                ) {
            nodeList = readFromXML.GetNodeList("nutrient", dataFromAPI.getData(d.getValue()));
            logger.info(TAG+" nodeList "+ nodeList);
            products = createModel.addToObject(nodeList, d.getValue(), d.getName());
            logger.info(TAG+" products "+products);
            writeToDatabase.createProducts(productsRepository, products);
        }
    }

    public void CreateMealsTable(MealsRepository mealsRepository, ProductsRepository productsRepository) {
        logger.info(TAG+" CreateMealsTable ");
        List<DataXML> list = readFromXML.readXMLTimeValue("meals.xml", "meal", "name", "products", "mealTime");
        logger.info(TAG+" List<DataXML> "+list);
        Meals meals;
        for (DataXML d : list
                ) {
            logger.info(TAG+" DataXML Name "+d.getName());
            logger.info(TAG+" DataXML Value "+d.getValue());
            meals = new Meals();
            meals.setName(d.getName());
            meals.setIdProducts("");
            meals.setMealTime(d.getValueTime());
            for (Products p : productsRepository.findAll()
                    ) {
                if (d.getValue().contains(p.getName())) {
                    logger.info(TAG+" Products "+p);
                    meals.setIdProducts(meals.getIdProducts() + p.getId() + ";");
                    meals.setEnergy(meals.getEnergy() + p.getEnergy());
                    meals.setCarbohydrates(meals.getCarbohydrates() + p.getCarbohydrates());
                    meals.setFats(meals.getFats() + p.getFats());
                    meals.setProtein(meals.getProtein() + p.getProtein());
                }
            }
            logger.info(TAG+" Meals "+meals);
            writeToDatabase.createMeals(mealsRepository, meals);
        }
    }

    public User buildUser(User user) {
        logger.info(TAG+" buildUser ");
        logger.info(TAG+" User "+user);
        return createModel.createUser(user);
    }

    public List<History> getHistory(String userId, HistoryRepository historyRepository) {
        logger.info(TAG+" getHistory ");
        logger.info(TAG+" userId "+userId);
        return historyRepository.findAll();
    }

    public List<Meals> getHistoryForUser(String userId, HistoryRepository historyRepository, MealsRepository mealsRepository) {
        logger.info(TAG+" getHistoryForUser ");
        List<History> list = historyRepository.findByUserid(Long.parseLong(userId));
        logger.info(TAG+" List<History> "+list);
        List<Meals> mealsList = new ArrayList<>();
        for (History h : list
                ) {
            mealsList.add(mealsRepository.findById(h.getMealsid()).get());
            logger.info(TAG+" History "+h);
        }
        return mealsList;
    }

    public List<String> addMealToHistory(String userId, String mealId, HistoryRepository historyRepository) {
        logger.info(TAG+" addMealToHistory ");
        List<History> historyList = historyRepository.findByUserid(Long.parseLong(userId));
        Date date = new Date();
        String TimeMeal=getTimeMeal(historyList,date);
        logger.info(TAG+" TimeMeal "+TimeMeal);
        History history = new History(Long.parseLong(userId), Long.parseLong(mealId));
        history.setPostedAt(date);
        history.setMealTime(TimeMeal);
        historyRepository.save(history);
        logger.info(TAG+" History "+history);
        return Arrays.asList(history.toString());

    }

    public List<String> getStaus(String userId, HistoryRepository historyRepository, UserRepository userRepository, MealsRepository mealsRepository) {
        logger.info(TAG+" getStatus ");
        List<History> list = historyRepository.findByUserid(Long.parseLong(userId));
        logger.info(TAG+" List<History> "+list);
        List<Meals> mealsList = new ArrayList<>();
        List<String> listStatus = new ArrayList<>();
        double energy = 0;
        double protein = 0;
        double carbohydrates = 0;
        double fats = 0;
        String TimeMeal="";
        Optional<User> user = userRepository.findById(Long.parseLong(userId));
        for (History h : list
                ) {
            mealsList.add(mealsRepository.findById(h.getMealsid()).get());
            TimeMeal+=h.getMealTime()+",";
        }
        listStatus.add("");
        for (Meals m : mealsList) {
            energy += m.getEnergy();
            protein += m.getProtein();
            carbohydrates += m.getCarbohydrates();
            fats += m.getFats();
            listStatus.add(m.toString());
            logger.info(TAG+" Meals "+m);
        }
        String status = user.get().getName() + " " + user.get().getSurname()+"\n "+TimeMeal +
                "\n Protein " + protein + "/" + user.get().getProtein() + "\n Carbohydrates " + carbohydrates + "/" + user.get().getCarbohydrates() +
                "\n Fats " + fats + "/" + user.get().getFats();
        listStatus.add(0, status);
        return listStatus;
    }

    public List<String> getMealToEat(String userId, MealsRepository mealsRepository, HistoryRepository historyRepository) {
        logger.info(TAG+" getMealToEat ");
        List<History> historyList = historyRepository.findByUserid(Long.parseLong(userId));
        logger.info(TAG+" List<History> "+historyList);
        Date date = new Date();
        String TimeMeal=getTimeMeal(historyList,date);
        List<Meals> mealsList=mealsRepository.findAll();
        logger.info(TAG+" List<Meals> "+mealsList);
        List<String> result=new ArrayList<>();
        for (Meals m:mealsList
             ) {
            if(m.getMealTime().contains(TimeMeal)){
                result.add(m.toString());
            }
        }
        return result;
    }
    private String getTimeMeal(List<History> historyList,Date date){
        logger.info(TAG+" getTimeMeal ");
        String TimeMeal="Śniadanie";
        for (History h : historyList) {
            if (h.getPostedAt().toString().substring(0, 10).equals(sdf.format(date).substring(0, 10))) {
                if (h.getMealTime().contains("Śniadanie")) {
                    TimeMeal="Obiad";
                } else if (h.getMealTime().contains("Obiad")) {
                    TimeMeal="Kolacja";
                } else if (h.getMealTime().contains("Kolacja")) {
                    TimeMeal="Kolacja";
                }
            }
        }
        logger.info(TAG+" TimeMeal "+TimeMeal);
        return TimeMeal;
    }
}
