package mealassistant.mealassistant.controller;

import lombok.extern.slf4j.Slf4j;
import mealassistant.mealassistant.model.Products;
import mealassistant.mealassistant.repository.ProductsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@Slf4j
@RestController
@RequestMapping("/api")
public class ProductController {

    final static Logger logger = LoggerFactory.getLogger(ProductController.class);
    final static String TAG= ProductController.class.getName();
    @Autowired
    ProductsRepository productsRepository;


    @RequestMapping(value = "/products",
            method = RequestMethod.GET,
            produces = {"application/json", "application/xml"})
    public List<Products> getAllProducts() {

        logger.info(TAG+" getAllProducts ");
        return productsRepository.findAll();
    }


}
