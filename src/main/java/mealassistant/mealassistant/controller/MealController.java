package mealassistant.mealassistant.controller;


import lombok.extern.slf4j.Slf4j;
import mealassistant.mealassistant.model.Meals;
import mealassistant.mealassistant.repository.HistoryRepository;
import mealassistant.mealassistant.repository.MealsRepository;
import mealassistant.mealassistant.service.Manager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Slf4j
@RestController
@RequestMapping("/api")
public class MealController {
    final static Logger logger = LoggerFactory.getLogger(ProductController.class);
    final static String TAG= ProductController.class.getName();

    @Autowired
    MealsRepository mealsRepository;

    @Autowired
    HistoryRepository historyRepository;

    @Autowired
    Manager manager;

    @RequestMapping(value = "/meals",
            method = RequestMethod.GET,
            produces = {"application/json", "application/xml"})
    public List<Meals> getAllMeals() {
        logger.info(TAG+" getAllMeals ");
        return mealsRepository.findAll();
    }

    @RequestMapping(value = "/addMeal",
            method = RequestMethod.GET,
            produces = {"application/json", "application/xml"},
            params = {"userid", "mealid"})
    public List<String> addMealToHistory(@RequestParam("userid") String UserId,
                                         @RequestParam("mealid") String MealId) {
        logger.info(TAG+" addMealToHistory ");
        logger.info(TAG+" UserId "+UserId);
        logger.info(TAG+" MealId "+MealId);
        return manager.addMealToHistory(UserId,MealId,historyRepository);
    }

    @RequestMapping(value = "/getMealToEat",
            method = RequestMethod.GET,
            produces = {"application/json", "application/xml"},
            params = {"userid"})
    public List<String> getMealToEat(@RequestParam("userid") String UserId) {
        logger.info(TAG+" getMealToEat ");
        logger.info(TAG+" UserId "+UserId);
        return manager.getMealToEat(UserId,mealsRepository,historyRepository);
    }

    @PostMapping("/meals")
    public Meals createMeals(@RequestBody Meals meals) {
        logger.info(TAG+" createMeals ");
        logger.info(TAG+" Meals "+meals);
        return mealsRepository.save(meals);
    }

}
