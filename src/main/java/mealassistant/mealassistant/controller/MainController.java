package mealassistant.mealassistant.controller;


import lombok.extern.slf4j.Slf4j;
import mealassistant.mealassistant.model.ApplicationUser;
import mealassistant.mealassistant.model.History;
import mealassistant.mealassistant.model.Meals;
import mealassistant.mealassistant.model.User;
import mealassistant.mealassistant.repository.HistoryRepository;
import mealassistant.mealassistant.repository.MealsRepository;
import mealassistant.mealassistant.repository.UserRepository;
import mealassistant.mealassistant.service.Manager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api")
public class MainController {

    final static Logger logger = LoggerFactory.getLogger(Controller.class);
    final static String TAG= Controller.class.getName();

    @Autowired
    UserRepository userRepository;

    @Autowired
    MealsRepository mealsRepository;

    @Autowired
    HistoryRepository historyRepository;

    @Autowired
    Manager manager;

    @RequestMapping(value = "/customer", method = RequestMethod.GET)
    public ApplicationUser getAddress() {

        ApplicationUser user=new ApplicationUser();
        user.setUsername("Jack");
        user.setPassword("US");

        return user;
    }

    @RequestMapping(value = "/users",
            method = RequestMethod.GET,
            produces = {"application/json", "application/xml"})
    public List<User> getAllUsers() {
        logger.info(TAG+" getAllUsers ");
        return userRepository.findAll();
    }


    @RequestMapping(value = "/history",
            method = RequestMethod.GET,
            produces = {"application/json", "application/xml"})
    public List<History> getAllHistory(@RequestParam("UserId") String UserId) {
        logger.info(TAG+" getAllHistory ");
        return manager.getHistory(UserId,historyRepository);
    }

    @RequestMapping(value = "/historyUser",
            method = RequestMethod.GET,
            produces = {"application/json", "application/xml"})
    public List<Meals> getAllHistoryUser(@RequestParam("UserId") String UserId) {
        logger.info(TAG+" getAllHistoryUser ");
        return manager.getHistoryForUser(UserId,historyRepository,mealsRepository);
    }

    @RequestMapping(value = "/getStatus",
            method = RequestMethod.GET,
            produces = {"application/json", "application/xml"},
            params = {"userid"})
    public List<String> getStatus(@RequestParam("userid") String UserId) {
        logger.info(TAG+" getStatus ");
        logger.info(TAG+" UserId "+UserId);
        return manager.getStaus(UserId,historyRepository,userRepository,mealsRepository);
    }

    @PostMapping("/users")
    public User createUser(@RequestBody User user) {

        logger.info(TAG+" createUser ");
        logger.info(TAG+" User "+user);
        return userRepository.save(manager.buildUser(user));
    }


    @RequestMapping(value = "/",
            method = RequestMethod.GET,
            produces = {"application/json", "application/xml"})
    public String index() {

        return "hello";
    }

}
